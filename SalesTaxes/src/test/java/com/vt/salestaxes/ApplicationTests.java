package com.vt.salestaxes;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.vt.salestaxes.domain.Item;
import com.vt.salestaxes.domain.Item.ItemBuilder;
import com.vt.salestaxes.domain.ItemTypeEnum;
import com.vt.salestaxes.domain.Receipt;
import com.vt.salestaxes.service.ReceiptService;
import com.vt.salestaxes.service.TaxesService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class ApplicationTests {
	
	@Autowired
	private TaxesService taxesService;
	
	@Autowired
	private ReceiptService receiptService;

	@Test
	public void getTaxPercentTest(){
		assertEquals(taxesService.getTaxPercent(new ItemBuilder().name("item").price(0D).build()), 
				BigDecimal.valueOf(10.00D).setScale(2));
		assertEquals(taxesService.getTaxPercent(new ItemBuilder().name("item").price(0D).type(ItemTypeEnum.BOOK).build()), 
				BigDecimal.ZERO);
		assertEquals(taxesService.getTaxPercent(new ItemBuilder().name("item").price(0D).type(ItemTypeEnum.MEDICAL).build()), 
				BigDecimal.ZERO);
		assertEquals(taxesService.getTaxPercent(new ItemBuilder().name("item").price(0D).type(ItemTypeEnum.FOOD).build()), 
				BigDecimal.ZERO);
		
		assertEquals(taxesService.getTaxPercent(new ItemBuilder().name("item").price(0D).imported().build()), 
				BigDecimal.valueOf(15.00D).setScale(2));
		assertEquals(taxesService.getTaxPercent(new ItemBuilder().name("item").price(0D).imported().type(ItemTypeEnum.BOOK).build()), 
				BigDecimal.valueOf(5.00D).setScale(2));
		assertEquals(taxesService.getTaxPercent(new ItemBuilder().name("item").price(0D).imported().type(ItemTypeEnum.MEDICAL).build()), 
				BigDecimal.valueOf(5.00D).setScale(2));
		assertEquals(taxesService.getTaxPercent(new ItemBuilder().name("item").price(0D).imported().type(ItemTypeEnum.FOOD).build()), 
				BigDecimal.valueOf(5.00D).setScale(2));
	}
	
	@Test
	public void computeTaxesTest(){
		assertEquals(taxesService.computeTaxes(new ItemBuilder().name("imported bottle of perfume").price(27.99D).imported().build()).getTax(),
				Double.valueOf(4.2D));
		assertEquals(taxesService.computeTaxes(new ItemBuilder().name("bottle of perfume").price(18.99D).build()).getTax(),
				Double.valueOf(1.9D));
		assertEquals(taxesService.computeTaxes(new ItemBuilder().name("packet of headache pills").price(9.75D).type(ItemTypeEnum.MEDICAL).build()).getTax(),
				Double.valueOf(0.0D));
		assertEquals(taxesService.computeTaxes(new ItemBuilder().name("box of imported chocolates").price(11.25D).type(ItemTypeEnum.FOOD).imported().build()).getTax(),
				Double.valueOf(0.6D));

	}
	
	@Test
	public void itemTaxesComputationTest1() {
		// imput1
		Item item1 = new ItemBuilder().name("book").price(12.49D).type(ItemTypeEnum.BOOK).build();
		Item item2 = new ItemBuilder().name("music CD").price(14.99D).build();
		Item item3 = new ItemBuilder().name("chocolate bar").price(0.85D).type(ItemTypeEnum.FOOD).build();
		Receipt receipt = new Receipt("", Arrays.asList(item1, item2, item3));
		receiptService.printReceipt(receipt);
		List<Item> items = receipt.getItems();
		for (Item item : items) {
			if(item.getName().equals("book")){
				assertEquals(item.getTotalPrice(), Double.valueOf(12.49D));
			}
			if(item.getName().equals("music CD")){
				assertEquals(item.getTotalPrice(), Double.valueOf(16.49D));
			}
			if(item.getName().equals("chocolate bar")){
				assertEquals(item.getTotalPrice(), Double.valueOf(0.85D));
			}
		}
		assertEquals(receipt.getSalesTaxes(), Double.valueOf(1.5D));
		assertEquals(receipt.getTotal(), Double.valueOf(29.83D));
	}

	@Test
	public void itemTaxesComputationTest2() {
		// imput2
		Item item1 = new ItemBuilder().name("box of chocolat").price(10.00D).type(ItemTypeEnum.FOOD).imported().build();
		Item item2 = new ItemBuilder().name("imported bottle of perfume").price(47.50D).imported().build();
		Receipt receipt = new Receipt("", Arrays.asList(item1, item2));
		receiptService.printReceipt(receipt);
		List<Item> items = receipt.getItems();
		for (Item item : items) {
			if(item.getName().equals("box of chocolat")){
				assertEquals(item.getTotalPrice(), Double.valueOf(10.50D));
			}
			if(item.getName().equals("imported bottle of perfume")){
				assertEquals(item.getTotalPrice(), Double.valueOf(54.65D));
			}
		}
		assertEquals(receipt.getSalesTaxes(), Double.valueOf(7.65D));
		assertEquals(receipt.getTotal(), Double.valueOf(65.15D));
	}
	
	@Test
	public void itemTaxesComputationTest3() {
		// imput3
		Item item1 = new ItemBuilder().name("imported bottle of perfume").price(27.99D).imported().build();
		Item item2 = new ItemBuilder().name("bottle of perfume").price(18.99D).build();
		Item item3 = new ItemBuilder().name("packet of headache pills").price(9.75D).type(ItemTypeEnum.MEDICAL).build();
		Item item4 = new ItemBuilder().name("box of imported chocolates").price(11.25D).type(ItemTypeEnum.FOOD).imported().build();
		Receipt receipt = new Receipt("", Arrays.asList(item1, item2, item3, item4));
		receiptService.printReceipt(receipt);
		List<Item> items = receipt.getItems();
		for (Item item : items) {
			if(item.getName().equals("imported bottle of perfume")){
				assertEquals(item.getTotalPrice(), Double.valueOf(32.19D));
			}
			if(item.getName().equals("bottle of perfume")){
				assertEquals(item.getTotalPrice(), Double.valueOf(20.89D));
			}
			if(item.getName().equals("packet of headache pills")){
				assertEquals(item.getTotalPrice(), Double.valueOf(9.75D));
			}
			if(item.getName().equals("box of imported chocolates")){
				assertEquals(item.getTotalPrice(), Double.valueOf(11.85D));
			}
		}
		assertEquals(receipt.getSalesTaxes(), Double.valueOf(6.70D));
		assertEquals(receipt.getTotal(), Double.valueOf(74.68D));
	}
	

}
