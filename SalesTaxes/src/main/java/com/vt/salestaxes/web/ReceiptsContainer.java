/**
 * 
 */
package com.vt.salestaxes.web;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import com.vt.salestaxes.domain.Receipt;

/**
 * @author VincentTortajada
 *
 */
@Component
@Scope(value="request", proxyMode=ScopedProxyMode.TARGET_CLASS)
public class ReceiptsContainer {

	private List<Receipt> receipts = new ArrayList<Receipt>();
	
	public ReceiptsContainer(){
		super();
	}
	
	/**
	 * Add a Receipt into the container
	 * @param receipt
	 * @return the container
	 */
	public ReceiptsContainer add(Receipt receipt) {
		this.receipts.add(receipt);
		return this;
	}
	
	/**
	 * @return the receipts
	 */
	public List<Receipt> getReceipts() {
		return receipts;
	}

	/**
	 * @param receipts the receipts to set
	 */
	public void setReceipts(List<Receipt> receipts) {
		this.receipts = receipts;
	}
}
