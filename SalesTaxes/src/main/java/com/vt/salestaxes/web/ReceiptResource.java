/**
 * 
 */
package com.vt.salestaxes.web;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vt.salestaxes.domain.Item;
import com.vt.salestaxes.domain.Item.ItemBuilder;
import com.vt.salestaxes.domain.ItemTypeEnum;
import com.vt.salestaxes.domain.Receipt;
import com.vt.salestaxes.service.ReceiptService;

/**
 * @author VincentTortajada
 * Receipt controller.
 */
@Controller
public class ReceiptResource {

	private static final Logger log = LoggerFactory.getLogger(ReceiptResource.class);
	
	@Autowired
	private ReceiptsContainer receiptsContainer;
	
	@Autowired
	private ReceiptService receiptService;
	
	@RequestMapping("/")
	public String index(){
		log.debug("/ endoint invoked -> index.html");
		// build receipts and add them into the container.
		// imput1
		Receipt buildReceipt1 = buildReceipt1();
		
		// imput2
		Receipt buildReceipt2 = buildReceipt2();
		
		// imput3
		Receipt buildReceipt3 = buildReceipt3();
		
		receiptsContainer.add(buildReceipt1).add(buildReceipt2).add(buildReceipt3);
		return "index";
	}

	/**
	 *  Build receipt "input1"
	 * @return input1 receipt
	 */
	private Receipt buildReceipt1() {
		Item item1 = new ItemBuilder().name("1 book").price(12.49D).type(ItemTypeEnum.BOOK).build();
		Item item2 = new ItemBuilder().name("1 music CD").price(14.99D).build();
		Item item3 = new ItemBuilder().name("1 chocolate bar").price(0.85D).type(ItemTypeEnum.FOOD).build();
		return receiptService.printReceipt(new Receipt("Output1:", Arrays.asList(item1, item2, item3)));
	}

	/**
	 *  Build receipt "input2"
	 * @return input1 receipt
	 */
	private Receipt buildReceipt2() {
		Item item1 = new ItemBuilder().name("1 box of chocolat").price(10.00D).type(ItemTypeEnum.FOOD).imported().build();
		Item item2 = new ItemBuilder().name("1 imported bottle of perfume").price(47.50D).imported().build();
		return receiptService.printReceipt(new Receipt("Output2:", Arrays.asList(item1, item2)));
	}

	/**
	 *  Build receipt "input3"
	 * @return input1 receipt
	 */
	private Receipt buildReceipt3() {
		Item item1 = new ItemBuilder().name("1 imported bottle of perfume").price(27.99D).imported().build();
		Item item2 = new ItemBuilder().name("1 bottle of perfume").price(18.99D).build();
		Item item3 = new ItemBuilder().name("1 packet of headache pills").price(9.75D).type(ItemTypeEnum.MEDICAL).build();
		Item item4 = new ItemBuilder().name("1 box of imported chocolates").price(11.25D).type(ItemTypeEnum.FOOD).imported().build();
		return receiptService.printReceipt(new Receipt("Output3:", Arrays.asList(item1, item2, item3, item4)));
	}
	
	/**
	 * @return the receipt
	 */
	@ModelAttribute("receiptsContainer")
	public ReceiptsContainer getReceipt() {
		return receiptsContainer;
	}
	
}
