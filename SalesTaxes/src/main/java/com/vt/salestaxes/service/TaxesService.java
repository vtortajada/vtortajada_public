/**
 * 
 */
package com.vt.salestaxes.service;

import java.math.BigDecimal;
import java.math.RoundingMode;

import com.vt.salestaxes.domain.Item;

/**
 * @author VincentTortajada
 * Provide services to compute Item taxes
 */
public interface TaxesService {

	/**
	 * Return the item with the tax value field updated.
	 * @param item {@link Item}
	 * @return The Item with tax value updated
	 */
	Item computeTaxes(Item item);

	/**
	 * Compute the item tax percent according to the item type and if the Item is imported.
	 * @param item {@link Item}
	 * @return tax percent
	 */
	BigDecimal getTaxPercent(Item item);
	
	
	/**
	 * Round the tax amount to the nearest X value and wanted rounding mode UP or DOWN
	 * @param taxes value
	 * @param divisor
	 * @param rounding mode UP or DOWN
	 * @return rounded tax value
	 */
	BigDecimal roundTax(BigDecimal tax, BigDecimal divisor, RoundingMode rounding);
	
	
}