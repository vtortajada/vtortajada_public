/**
 * 
 */
package com.vt.salestaxes.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vt.salestaxes.domain.Item;
import com.vt.salestaxes.domain.Receipt;

/**
 * @author VincentTortajada Provide Receipt services
 */
@Service
public class ReceiptService {
	
	private static final Logger log = LoggerFactory.getLogger(ReceiptService.class);
	
	@Autowired
	private TaxesService taxesService;

	/**
	 * Compute a receipt, 
	 * compute first each item tax and total price,
	 * then compute receipt taxes and total price.
	 * @param receipt 
	 * @return Computed receipt
	 */
	public Receipt printReceipt(Receipt receipt) {
		log.debug("Start printReceipt for empty receipt: "+receipt);
		List<Item> items = receipt.getItems();
		if(items != null){
			// compute each Items
			for (Item item : items) {
				taxesService.computeTaxes(item);
			}
		}
		// compute and return receipt
		Receipt computedReceipt = receipt.computeReceipt();
		log.debug("End printReceipt, computed receipt: "+computedReceipt);
		return computedReceipt;
	}
}
