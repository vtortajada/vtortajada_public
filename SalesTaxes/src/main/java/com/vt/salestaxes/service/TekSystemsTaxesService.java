/**
 * 
 */
package com.vt.salestaxes.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import com.vt.salestaxes.domain.Item;
import com.vt.salestaxes.domain.ItemTypeEnum;

/**
 * @author VincentTortajada
 * {@see TaxesService}
 * Default implementation
 */
@Service
@Validated
public class TekSystemsTaxesService implements TaxesService {
	
	private static final Logger log = LoggerFactory.getLogger(ReceiptService.class);
	
	private static final int DIGITS_NUMBER = 2;
	
	private static final BigDecimal HUNDRED = new BigDecimal(100);

	@Value("${tax.default.percent}")
	private BigDecimal taxDefaultPercent;
	@Value("${tax.imported.percent}")
	private BigDecimal taxImportedPercent;
	@Value("#{'${tax.exceptions}'.split(',')}")
	private List<ItemTypeEnum> taxExceptions;
	@Value("${tax.nearest.value}")
	private BigDecimal nearestValue;
	@Value("${tax.rounding.mode}")
	private RoundingMode roundingMode;
	


	/* 
	 * @see com.vt.salestaxes.service.TaxesService#computeTaxes(com.vt.salestaxes.domain.Item)
	 */
	@Override
	public Item computeTaxes(@NotNull Item item) {
		log.debug("Starting computing taxes for Item: "+item);
		BigDecimal taxPercent = getTaxPercent(item);
		
		//calculate the tax amount if the tax percent is not 0
		if(taxPercent.signum() != 0){
			BigDecimal taxes = BigDecimal.valueOf(item.getPrice()).multiply(taxPercent)
					.divide(HUNDRED, DIGITS_NUMBER, BigDecimal.ROUND_HALF_UP);
			log.debug("Computed tax equals: "+taxes +" for tax percent "+ taxPercent+ " and Item "+item);
			// round to the nearest X with N rounding mode
			taxes = roundTax(taxes, nearestValue, roundingMode);
			log.debug("Computed tax after rounded = "+taxes);
			item.setTaxes(taxes.doubleValue());
		}
		log.debug("Taxes computed for Item: "+item);
		return item;
	}
	
	/* 
	 * @see com.vt.salestaxes.service.TaxesService#getTaxPercent(com.vt.salestaxes.domain.Item)
	 */
	public BigDecimal getTaxPercent(@NotNull @Valid Item item) {
		BigDecimal itemTaxPercent = BigDecimal.ZERO;
		// first the default tax
		// check if the Item is a tax free Item otherwise add default tax
		if(taxExceptions == null || !taxExceptions.contains(item.getType())){
			itemTaxPercent = itemTaxPercent.add(taxDefaultPercent).setScale(DIGITS_NUMBER);
		}
		
		// imported tax
		if(item.getImported()) itemTaxPercent = itemTaxPercent.add(taxImportedPercent).setScale(DIGITS_NUMBER);
		log.debug("Tax percent equals: "+itemTaxPercent+" for item "+item);
		return itemTaxPercent;
	}
	
	
	/**
	 * @see com.vt.salestaxes.service.TaxesService#roundTax(BigDecimal, BigDecimal, RoundingMode)
	 */
	public BigDecimal roundTax(@NotNull BigDecimal tax, @NotNull BigDecimal divisor, @NotNull RoundingMode rounding) {
		// divisor has to be positive
		if(divisor.signum() != 1){
			throw new IllegalArgumentException("divisor argument must be positive.");
		}
		// get the number of "divisor" contained in the tax amount with the chosen rounding mode.
		BigDecimal divided = tax.divide(divisor, 0, rounding);
		// one divided and rounded multiply back by the divisor (and add the scale for $ prices)
		return divided.multiply(divisor).setScale(DIGITS_NUMBER, RoundingMode.DOWN);
	}

}
