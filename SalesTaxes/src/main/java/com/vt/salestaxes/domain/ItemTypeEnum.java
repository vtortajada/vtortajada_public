package com.vt.salestaxes.domain;

/**
 * Item type.
 * @author VincentTortajada
 *
 */
public enum ItemTypeEnum {

	BOOK, FOOD, MEDICAL, OTHER
}
