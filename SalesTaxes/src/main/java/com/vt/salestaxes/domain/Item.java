package com.vt.salestaxes.domain;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.vt.salestaxes.tools.BigDecimalTool;

/**
 * A sale item.
 * 
 * @author VincentTortajada
 *
 */
public class Item {

	/**
	 * Item name.
	 */
	@NotNull
	private final String name;

	/**
	 * Item prize without taxes
	 */
	@NotNull
	@Min(0)
	private final Double price;

	/**
	 * Item tax amount
	 */
	private Double tax = 0D;

	/**
	 * Item type {@link ItemTypeEnum}
	 */
	@NotNull
	private final ItemTypeEnum type;

	/**
	 * True if Item is imported.
	 */
	@NotNull
	private final Boolean imported;

	/**
	 * Use {@link ItemBuilder}
	 * 
	 * @param name
	 * @param price
	 * @param type
	 * @param imported
	 */
	private Item(String name, Double price, ItemTypeEnum type,
			Boolean imported) {
		super();
		this.name = name;
		this.price = price;
		this.type = type;
		this.imported = imported;
	}

	/**
	 * @return the tax
	 */
	public Double getTax() {
		return tax;
	}

	/**
	 * @param tax
	 *            the tax to set
	 */
	public void setTaxes(Double tax) {
		this.tax = tax;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the price
	 */
	public Double getPrice() {
		return price;
	}

	/**
	 * @return the type
	 */
	public ItemTypeEnum getType() {
		return type;
	}

	/**
	 * @return the imported
	 */
	public Boolean getImported() {
		return imported;
	}
	
	/**
	 * Compute total price: price + taxes
	 * @return Total price
	 */
	public Double getTotalPrice(){
		return BigDecimalTool.addDouble(price, tax, 2);
	}
	
	/**
	 * Builder for {@link Item}
	 * @author VincentTortajada
	 *
	 */
	@Validated
	public static class ItemBuilder {
		/**
		 * Item name.
		 */
		private String name;

		/**
		 * Item prize without taxes
		 */
		private Double price;

		/**
		 * Item type {@link ItemTypeEnum}
		 */
		private ItemTypeEnum type = ItemTypeEnum.OTHER;

		/**
		 * True if Item is imported.
		 */
		private Boolean imported = Boolean.FALSE;
		
		/**
		 * Add name to the builder
		 * @param name
		 * @return Builder
		 */
		public ItemBuilder name(@NotNull String name){
			this.name = name;
			return this;
		}
		
		/**
		 * Add price to the builder
		 * @param price
		 * @return Builder
		 */
		public ItemBuilder price(@NotNull @Min(0)Double price){
			this.price = price;
			return this;
		}
		
		/**
		 * Add type to the builder
		 * @param type
		 * @return Builder
		 */
		public ItemBuilder type(@NotNull ItemTypeEnum type){
			this.type = type;
			return this;
		}
		
		/**
		 * Add imported to the builder
		 * @param imported
		 * @return Builder
		 */
		public ItemBuilder imported(){
			this.imported = Boolean.TRUE;
			return this;
		}
		
		public Item build(){
			return new Item(name, price, type, imported);
		}
	}
	
	/* 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Item [name=" + name + ", price=" + price + ", tax=" + tax
				+ ", type=" + type + ", imported=" + imported + ", total=" + getTotalPrice() +"]";
	}
}
