/**
 * 
 */
package com.vt.salestaxes.domain;

import java.util.List;

import javax.validation.constraints.Min;

import com.vt.salestaxes.tools.BigDecimalTool;

/**
 * @author VincentTortajada A Item receipt.
 */
public class Receipt {

	/**
	 * The name of the receipt
	 */
	private String name;

	/**
	 * The list of purchased items.
	 */
	private List<Item> items;

	/**
	 * Taxes amount for all the items.
	 */
	@Min(0)
	private Double salesTaxes;

	/**
	 * Total price of the items.
	 */
	@Min(0)
	private Double total;

	/**
	 * Create a Receipt with a list of Items
	 * @param items List of Items
	 */
	public Receipt(String name, List<Item> items) {
		super();
		this.name = name;
		this.items = items;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return the items
	 */
	public List<Item> getItems() {
		return items;
	}

	/**
	 * @param items
	 *            the items to set
	 */
	public void setItems(List<Item> items) {
		this.items = items;
	}

	/**
	 * @return the salesTaxes
	 */
	public Double getSalesTaxes() {
		return salesTaxes;
	}

	/**
	 * @param salesTaxes
	 *            the salesTaxes to set
	 */
	public void setSalesTaxes(Double salesTaxes) {
		this.salesTaxes = salesTaxes;
	}

	/**
	 * @return the total
	 */
	public Double getTotal() {
		return total;
	}

	/**
	 * @param total
	 *            the total to set
	 */
	public void setTotal(Double total) {
		this.total = total;
	}

	/**
	 * Compute the taxes and total amount of the receipt.
	 * @return computed {@link Receipt}
	 */
	public Receipt computeReceipt() {
		// first re initialize the receipt to prevent multiples calls
		this.salesTaxes = 0D;
		this.total = 0D;
		// for each Item add item tax add total cost to the receipt
		if(items != null) {
			for (Item item : items) {
				salesTaxes = BigDecimalTool.addDouble(salesTaxes, item.getTax(), 2);
				total = BigDecimalTool.addDouble(total, item.getTotalPrice(), 2);
			}
		}
		return this;
	}

	/* 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Receipt [name=" + name + ", items=" + items + ", salesTaxes="
				+ salesTaxes + ", total=" + total + "]";
	}

}
