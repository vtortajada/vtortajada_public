/**
 * 
 */
package com.vt.salestaxes.tools;

import java.math.BigDecimal;

/**
 * @author VincentTortajada
 * Tools class for BigDecimal operations
 */
public class BigDecimalTool {

	/**
	 * Add 2 double a and b, the result will have N digits
	 * @param a
	 * @param b
	 * @param nb digits
	 * @return a + b
	 */
	public static Double addDouble(Double a, Double b, int nbDigits) {
		return BigDecimal.valueOf(a).add(BigDecimal.valueOf(b)).setScale(nbDigits).doubleValue();
	}
}
